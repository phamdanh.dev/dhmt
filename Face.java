package com.phamdanh.dev;

import java.awt.Frame;

import jgl.GL;
import jgl.GLCanvas;

public class Face extends GLCanvas {

	private void draw_face() {
		myGL.glBegin(GL.GL_LINE_LOOP);
		myGL.glVertex2f(-25f, 45f);
		myGL.glVertex2f(0f, 50f);
		myGL.glVertex2f(25f, 45f);
		myGL.glVertex2f(45f, 25f);
		myGL.glVertex2f(45f, 0f);
		myGL.glVertex2f(20f, -50f);
		myGL.glVertex2f(-20f, -50f);
		myGL.glVertex2f(-45f, 0f);
		myGL.glVertex2f(-45f, 25f);
		myGL.glEnd();
	}

	private void draw_left_eye() {
		myGL.glBegin(GL.GL_LINE_LOOP);
		myGL.glVertex2f(-30f, 20f);
		myGL.glVertex2f(-15f, 20f);
		myGL.glVertex2f(-12f, 15f);
		myGL.glVertex2f(-15f, 10f);
		myGL.glVertex2f(-30f, 10f);
		myGL.glVertex2f(-33f, 15f);
		myGL.glEnd();
	}

	private void draw_right_eye() {
		myGL.glBegin(GL.GL_LINE_LOOP);
		myGL.glVertex2f(30f, 20f);
		myGL.glVertex2f(15f, 20f);
		myGL.glVertex2f(12f, 15f);
		myGL.glVertex2f(15f, 10f);
		myGL.glVertex2f(30f, 10f);
		myGL.glVertex2f(33f, 15f);
		myGL.glEnd();
	}

	private void draw_nose() {
		myGL.glBegin(GL.GL_LINE_LOOP);
		myGL.glVertex2f(0f, 5f);
		myGL.glVertex2f(8f, -10f);
		myGL.glVertex2f(0f, -13f);
		myGL.glVertex2f(-8f, -10f);
		myGL.glVertex2f(0f, 5f);
		myGL.glEnd();
	}

	private void draw_mouth() {
		myGL.glBegin(GL.GL_LINE_LOOP);
		myGL.glVertex2f(-20f, -25f);
		myGL.glVertex2f(-20f, -30f);
		myGL.glVertex2f(20f, -30f);
		myGL.glVertex2f(20f, -25f);
		myGL.glEnd();
	}

	public void display() {
		myGL.glClear(GL.GL_COLOR_BUFFER_BIT); // Xoa cac pixel
		myGL.glColor3f(1.0f, 1.0f, 1.0f);

		/*
		 * Ham tao hinh o day
		 */
		draw_face();
		draw_left_eye();
		draw_right_eye();
		draw_nose();
		draw_mouth();

		// chay luon ma khong can doi
		myGL.glFlush();
	}

	// Thiet lap khoi tao ca nhan
	private void myInit() {
		// Chon mau khi Clear
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Mau den
		// Khoi tao gia tri viewing
		myGL.glMatrixMode(GL.GL_PROJECTION); // Thong bao ma tran nao
		myGL.glLoadIdentity(); // Thiet lap gia tri ma tran don vi vao ma tran phep chieu
		myGL.glOrtho(-100f, 100f, -100f, 100f, 0.0f, 0.0f); // Tao he truc toa do

	}

	// Khoi tao man hinh
	public void init() {
		myUT.glutInitWindowSize(500, 500); // Kich thuoc cua so
		myUT.glutInitWindowPosition(0, 0); // Goc hien thi cua so
		myUT.glutCreateWindow(this);
		myInit();
		myUT.glutDisplayFunc("display");
		myUT.glutMainLoop();

	}

	public static void main(String[] args) {
		Frame mainFrame = new Frame();
		mainFrame.setSize(508, 527);
		Face mainCanvas = new Face();
		mainCanvas.init();
		mainFrame.add(mainCanvas);
		mainFrame.setVisible(true);
	}
}
