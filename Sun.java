package com.phamdanh.dev;

import java.awt.Frame;

import jgl.GL;
import jgl.GLCanvas;

public class Sun extends GLCanvas {


	private void draw_circle(double r) {
		myGL.glBegin(GL.GL_LINE_LOOP);
		for (double degrees = 0.0; degrees < 360.0; degrees += 0.05) {
			double radians = Math.toRadians(degrees);
			myGL.glVertex2d(r*Math.cos(radians), r*Math.sin(radians));
		}
		
		myGL.glEnd();
	}
	
	private void draw_tamGiac(double rLon, double rNho) {
		myGL.glBegin(GL.GL_LINE_STRIP);
		int count = 0;
		for (double radians = 0.0; radians < (Math.PI*2 + Math.PI/7); radians += (Math.PI/6)) {
			count ++;
			if (count%2 == 0) {
				myGL.glVertex2d(rNho*Math.cos(radians), rNho*Math.sin(radians));
			} else {
				myGL.glVertex2d(rLon*Math.cos(radians), rLon*Math.sin(radians));
			}
			
		}
		
		myGL.glEnd();
		
	}

	public void display() {
		myGL.glClear(GL.GL_COLOR_BUFFER_BIT);
		myGL.glColor3f(1.0f, 1.0f, 1.0f);

		/*
		 * Ham tao hinh o day
		 */
		double rLon = 90;
		double rNho = 52;
		draw_circle(rLon);
		draw_circle(rNho);
		draw_tamGiac(rLon, rNho);

		// chay luon ma khong can doi
		myGL.glFlush();
	}

	

	// Thiet lap khoi tao ca nhan
	private void myInit() {
		// Chon mau khi Clear
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Mau den
		// Khoi tao gia tri viewing
		myGL.glMatrixMode(GL.GL_PROJECTION); // Thong bao ma tran nao
		myGL.glLoadIdentity(); // Thiet lap gia tri ma tran don vi vao ma tran phep chieu
		myGL.glOrtho(-100f, 100f, -100f, 100f, 0.0f, 0.0f); // Tao he truc toa do

	}

	// Khoi tao man hinh
	public void init() {
		myUT.glutInitWindowSize(500, 500); // Kich thuoc cua so
		myUT.glutInitWindowPosition(0, 0); // Goc hien thi cua so
		myUT.glutCreateWindow(this);
		myInit();
		myUT.glutDisplayFunc("display");
		myUT.glutMainLoop();

	}

	public static void main(String[] args) {
		Frame mainFrame = new Frame();
		mainFrame.setSize(508, 527);
		Sun mainCanvas = new Sun();
		mainCanvas.init();
		mainFrame.add(mainCanvas);
		mainFrame.setVisible(true);
	}
}
