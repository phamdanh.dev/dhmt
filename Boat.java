package com.phamdanh.dev;

/*  bezmesh.java
*  This program renders a lighted, filled Bezier surface,
*  using two-dimensional evaluators.
*/

import java.awt.Frame;
import java.io.IOException;

import jgl.GL;
import jgl.GLCanvas;

public class Boat extends GLCanvas {

	private static final float ctrlpoints[][][] = {
			{ { 0f, 3f, 0f }, { 0f, 3f, 0f }, { 0f, 3f, 0f }, { 0f, 3f, 0f } },
			{ { -1.5f, 2.5f, 0f }, { -1.5f, 2.5f, -1.5f }, { 1.5f, 2.5f, -1.5f }, { 1.5f, 2.5f, 0f } },
			{ { -1.5f, -2.5f, 0f }, { -1.5f, -2.5f, -1.5f }, { 1.5f, -2.5f, -1.5f }, { 1.5f, -2.5f, 0f } },
			{ { 0f, -3f, 0f }, { 0f, -3f, 0f }, { 0f, -3f, 0f }, { 0f, -3f, 0f } } };

	private static float rotate = -100, x = 1, y = 0.7f, xTranslate = 0.0f;

	private void rotateTop() {
//		rotate = rotate + 5;
//		x = 1;
//		y = 0;
	}

	private void rotateBottom() {
//		rotate = rotate - 5;
//		x = 1;
//		y = 0;
	}
	
	private void rotateLeft() {
//		rotate = rotate + 5;
//		x = 0;
//		y = 1;
		y = y + 0.1f;
	}

	private void rotateRight() {
//		rotate = rotate - 5;
//		x = 0;
//		y = 1;
		y = y - 0.1f;
	}
	
	
	float nAnimate = 0.00f;
	int flagAnimate = 0;
	private void animate() throws InterruptedException {
		
		if (flagAnimate == 0) {
			nAnimate += 0.002;
			y = y + nAnimate;
			if (nAnimate >= 0.03) {
				flagAnimate = 1;
				nAnimate = 0;
			}
			xTranslate += 0.02f;
			display();
			repaint();
			return;
		}
		if (flagAnimate == 1) {
			nAnimate += 0.002;
			y = y - nAnimate;
			if (nAnimate >= 0.03) {
				flagAnimate = 0;
				nAnimate = 0;
			}
			xTranslate -= 0.02f;
			display();
			repaint();
			return;
		}
			
	}


	public void display() {
		float ambient[] = { 131.0f/255.0f, 84.0f/255.0f, 38.0f/255.0f, 1.0f };
		float diffuse[] = { 131.0f/255.0f, 84.0f/255.0f, 38.0f/255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = {57.0f/255.0f, 36.0f/255.0f, 15.0f/255.0f, 1.0f};
		float shininess[] = { 20 };
		
		myGL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
//		myGL.glPushMatrix();
		myGL.glRotatef((float) rotate, x, y, 1.0f); // 85, 1,1,1
		myGL.glTranslatef( 0, 0, xTranslate);
		myGL.glEvalMesh2(GL.GL_FILL, 0, 20, 0, 20);
		myGL.glPopMatrix();
		myGL.glFlush();
	}
	
	 private void initlights () {
			float ambient[] = {0.2f, 0.2f, 0.2f, 0.0f};
			float position[] = {50.0f, -50.0f, 40.0f, 1.0f};// 0, -2, 4
			float mat_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
			float mat_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
		        float mat_shininess[] = {50.0f};

		        myGL.glEnable (GL.GL_LIGHTING);
		        myGL.glEnable (GL.GL_LIGHT0);

		        myGL.glLightfv (GL.GL_LIGHT0, GL.GL_AMBIENT, ambient);
		        myGL.glLightfv (GL.GL_LIGHT0, GL.GL_POSITION, position);

		        myGL.glMaterialfv (GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
		        myGL.glMaterialfv (GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
		        myGL.glMaterialfv (GL.GL_FRONT, GL.GL_SHININESS, mat_shininess);
		    }

	private void myinit() {
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		myGL.glEnable(GL.GL_DEPTH_TEST);
		myGL.glMap2f(GL.GL_MAP2_VERTEX_3, 0.0f, 1.0f, 3, 4, 0.0f, 1.0f, 12, 4, ctrlpoints);
		myGL.glEnable(GL.GL_MAP2_VERTEX_3);
		myGL.glEnable(GL.GL_AUTO_NORMAL);
		myGL.glEnable(GL.GL_NORMALIZE);
		myGL.glMapGrid2f(20, 0.0f, 1.0f, 20, 0.0f, 1.0f);
		initlights ();
	}

	public void myReshape(int w, int h) {
		myGL.glViewport(0, 0, w, h);
		myGL.glMatrixMode(GL.GL_PROJECTION);
		myGL.glLoadIdentity();
		if (w <= h) {
			myGL.glOrtho(-4.0f, 4.0f, -4.0f * (float) h / (float) w, 4.0f * (float) h / (float) w, -4.0f, 4.0f);
		} else {
			myGL.glOrtho(-4.0f * (float) w / (float) h, 4.0f * (float) w / (float) h, -4.0f, 4.0f, -4.0f, 4.0f);
		}
		myGL.glMatrixMode(GL.GL_MODELVIEW);
		myGL.glLoadIdentity();
	}

	public void keyboard(char key, int x, int y) throws InterruptedException {
		switch (key) {
		case 27:
			System.exit(0);
		case ' ':
			System.out.println("SPACE");
			animate();
			break;
		case 'a':
			System.out.println("A");
			rotateLeft();
			display();
			repaint();
			break;
		case 'd':
			System.out.println("D");
			rotateRight();
			display();
			repaint();
			break;
		default:
			break;
		}
	}

	public void init() {
		myUT.glutInitWindowSize(500, 500);
		myUT.glutInitWindowPosition(0, 0);
		myUT.glutCreateWindow(this);
		myinit();
		myUT.glutReshapeFunc("myReshape");
		myUT.glutDisplayFunc("display");
		myUT.glutKeyboardFunc("keyboard");
		myUT.glutMainLoop();
	}

	static public void main(String args[]) throws IOException {
		Frame mainFrame = new Frame();
		mainFrame.setSize(508, 527);
		Boat mainCanvas = new Boat();
		mainCanvas.init();
		mainFrame.add(mainCanvas);
		mainFrame.setVisible(true);
	}

}
