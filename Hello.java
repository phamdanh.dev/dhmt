package com.phamdanh.dev;

import java.awt.Frame;

import jgl.GL;
import jgl.GLCanvas;

public class Hello extends GLCanvas {
	public void display() {
		myGL.glClear(GL.GL_COLOR_BUFFER_BIT); // Xoa cac pixel
		/*
		 * Ve mot polygon(da giac) la mot rectangle(hinh chu nhat) tai goc 0.25 va 0.75
		 * co mau trang
		 */
		myGL.glColor3f(1.0f, 1.0f, 1.0f);
		myGL.glBegin(GL.GL_POLYGON); // Bat dau cham cac diem
		myGL.glVertex3f(0.25f, 0.25f, 0.0f);
		myGL.glVertex3f(0.75f, 0.25f, 0.0f);
		myGL.glVertex3f(0.75f, 0.75f, 0.0f);
		myGL.glVertex3f(0.25f, 0.75f, 0.0f);
		myGL.glEnd();
		// chay luon ma khong can doi
		myGL.glFlush();
	}
	
	public void keyboard (char key, int x, int y) {
		switch (key) {
		    case 27:
			System.exit(0);
		    default:
			break;
		}
	    }

	// Thiet lap khoi tao ca nhan
	private void myInit() {
		// Chon mau khi Clear
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Mau den
		// Khoi tao gia tri viewing
		myGL.glMatrixMode(GL.GL_PROJECTION); // Thong bao ma tran nao
		myGL.glLoadIdentity(); // Thiet lap gia tri ma tran don vi vao ma tran phep chieu
		myGL.glOrtho(0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f); // Tao he truc toa do

	}

	// Khoi tao man hinh
	public void init() {
		myUT.glutInitWindowSize(500, 500); // Kich thuoc cua so
		myUT.glutInitWindowPosition(0, 0); // Goc hien thi cua so
		myUT.glutCreateWindow(this);
		myInit();
		myUT.glutDisplayFunc("display");
		myUT.glutKeyboardFunc ("keyboard");
		myUT.glutMainLoop();

	}
	
	public static void main(String[] args) {
		Frame mainFrame = new Frame();
		mainFrame.setSize(508, 527);
		Hello mainCanvas = new Hello();
		mainCanvas.init();
		mainFrame.add(mainCanvas);
		mainFrame.setVisible(true);
	}
}
