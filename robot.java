package com.phamdanh.dev;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import jgl.GL;
import jgl.GLAUX;
import jgl.GLU;
import jgl.GLUT;
import jgl.glu.GLUquadricObj;

public class robot extends Applet implements ComponentListener, KeyListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// must use GL to use jGL.....
	// and use GLU to use the glu functions....
	// remember to give GL to initialize GLU
	// and use GLAUX to use the aux functions.....
	// remember to give GL to initialize GLAUX
	GL myGL = new GL();
	GLU myGLU = new GLU(myGL);
	GLAUX myAUX = new GLAUX(myGL);
	GLUT myUT = new GLUT(myGL);

	private static float shoulderLeft = 0, elbowLeft = 0, shoulderRight = 0, elbowRight = 0, legLeft = 0, legRight = 0,
			look = 0, xLeg = 0, zLeg = 0, cangTrai = 0, cangPhai = 0, rTranslateX = 0, rTranslateZ = 0, bTranslateX = 0,
			bTranslateY = 0, bTranslateZ = 0;

	private void elbowLeftAdd() {
		elbowLeft = (elbowLeft + 5) % 360;
	}

	private void elbowLeftSubtract() {
		elbowLeft = (elbowLeft - 5) % 360;
	}

	private void shoulderLeftAdd() {
		shoulderLeft = (shoulderLeft + 5) % 360;
	}

	private void shoulderLeftSubtract() {
		shoulderLeft = (shoulderLeft - 5) % 360;
	}

	private void elbowRightAdd() {
		elbowRight = (elbowRight + 5) % 360;
	}

	private void elbowRightSubtract() {
		elbowRight = (elbowRight - 5) % 360;
	}

	private void shoulderRightAdd() {
		shoulderRight = (shoulderRight + 5) % 360;
	}

	private void shoulderRightSubtract() {
		shoulderRight = (shoulderRight - 5) % 360;
	}

	private void legRightAdd() {
		legRight = (legRight + 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legRightSubtract() {
		legRight = (legRight - 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legLeftAdd() {
		legLeft = (legLeft + 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legLeftSubtract() {
		legLeft = (legLeft - 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	// a
	private void legRightFront() {
		legRight = (legRight + 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void legRightBack() {
		legRight = (legRight - 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void legLeftFront() {
		legLeft = (legLeft + 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void legLeftBack() {
		legLeft = (legLeft - 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void cangTraiFront() {
		cangTrai = (cangTrai + 5) % 360;
	}

	private void cangTraiBack() {
		cangTrai = (cangTrai - 5) % 360;
	}

	private void cangPhaiFront() {
		cangPhai = (cangPhai + 5) % 360;
	}

	private void cangPhaiBack() {
		cangPhai = (cangPhai - 5) % 360;
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void lookLeft() {
		look = look - 0.1f;
	}

	public void lookRight() {
		look = look + 0.1f;
	}

	// RESET
	public void reset() {
		shoulderLeft = 0;
		elbowLeft = 0;
		shoulderRight = 0;
		elbowRight = 0;
		legLeft = 0;
		legRight = 0;
		look = 0;
		xLeg = 0;
		zLeg = 0;
		cangTrai = 0;
		cangPhai = 0;
		rTranslateX = 0;
		rTranslateZ = 0;
		bTranslateX = 0;
		bTranslateY = 0;
		bTranslateZ = 0;
		countKick = 0;
	}

	// KICK BALL

	int countKick = 0;

	public void kickBall() {
		countKick++;
		System.out.println(countKick);
		// CHAN TRAI
		if (countKick <= 10) {
			legLeftFront();
			cangTraiFront();
		}
		if (countKick > 10 && countKick <= 30) {
			legLeftBack();
			if (countKick < 21) {
				cangTraiBack();
			}
		}
		if (countKick > 30 && countKick <= 40) {
			legLeftFront();
			legRightBack();
		}
		// END CHAN TRAI

		// CHAN PHAI

		if (countKick <= 10) {
			legRightBack();
			cangPhaiFront();
		}
		if (countKick > 10 && countKick <= 30) {
			legRightFront();
			if (countKick < 21) {
				cangPhaiBack();
			}
		}

		// END CHAN PHAI

		// CHAY
		if (countKick < 40) {
			rTranslateX += 0.01;
			rTranslateZ += 0.04;
		}
		// END CHAY

		// TAY TRAI

		if (countKick <= 10) {
			shoulderLeftAdd();
			elbowLeftAdd();
		}

		if (countKick > 10 && countKick <= 30) {
			shoulderLeftSubtract();
			if (countKick < 22) {
				elbowLeftSubtract();
			}
		}
		if (countKick > 30 && countKick <= 50) {
			shoulderLeftAdd();
			elbowLeftAdd();
		}
		if (countKick > 50 && countKick <= 60) {
			elbowLeftSubtract();
		}
		if (countKick > 60 && countKick <= 70) {
			elbowLeftAdd();
		}
		if (countKick > 70 && countKick <= 80) {
			elbowLeftSubtract();
		}
		if (countKick > 80 && countKick <= 90) {
			elbowLeftAdd();
		}

		// END TAY TRAI

		// TAY PHAI

		if (countKick <= 10) {
			shoulderRightAdd();
		}

		if (countKick > 10 && countKick <= 30) {
			shoulderRightSubtract();
			elbowRightSubtract();
		}
		if (countKick > 50 && countKick <= 60) {
			elbowRightAdd();
		}
		if (countKick > 60 && countKick <= 70) {
			elbowRightSubtract();
		}
		if (countKick > 70 && countKick <= 80) {
			elbowRightAdd();
		}
		if (countKick > 80 && countKick <= 90) {
			elbowRightSubtract();
		}

		// END TAY PHAI

		// BALL
		
		if (countKick > 25) {
			bTranslateX += 0.05;
			bTranslateY += 0.2;
			bTranslateZ += 0.2;
		}

		// END BALL
	}

	// END KICK BALL

	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();

		if (keyCode == KeyEvent.VK_Q) {
			shoulderLeftAdd();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_W) {
			shoulderLeftSubtract();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_R) {
			elbowLeftSubtract();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_E) {
			elbowLeftAdd();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_O) {
			shoulderRightAdd();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_P) {
			shoulderRightSubtract();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_U) {
			elbowRightAdd();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_I) {
			elbowRightSubtract();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_A) {
			legLeftSubtract();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_S) {
			legLeftAdd();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_K) {
			legRightSubtract();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_L) {
			legRightAdd();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_J) {
			legRightBack();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_H) {
			legRightFront();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_F) {
			legLeftBack();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_D) {
			legLeftFront();
			display();
			repaint();
			componentResized(e);
		}

		else if (keyCode == KeyEvent.VK_LEFT) {
			lookLeft();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_RIGHT) {
			lookRight();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_Z) {
			cangTraiFront();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_X) {
			cangTraiBack();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_M) {
			cangPhaiFront();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_N) {
			cangPhaiBack();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_SPACE) {
			kickBall();
			display();
			repaint();
			componentResized(e);
		} else if (keyCode == KeyEvent.VK_BACK_SPACE) {
			reset();
			display();
			repaint();
			componentResized(e);
		}

		e.consume();
	}

	private void display() {
		myGL.glPushMatrix();
		myGL.glRotatef(50.0f, 0.0f, 1.0f, 0.0f);
		myGL.glTranslatef(-0.8f, 0.0f, -2f);
		myGL.glTranslatef(rTranslateX, 0.0f, rTranslateZ);
		drawBody();
		drawRightArm();
		drawLeftArm();
		drawLeftLeg();
		drawRightLeg();
		drawHead();
		myGL.glPopMatrix();
		myGL.glPushMatrix();
		myGL.glTranslatef(bTranslateX, bTranslateY, bTranslateZ);
		drawBall();
		myGL.glPopMatrix();
		myGL.glFlush();
	}

	/*
	 * MY CODE
	 */

	public void drawBody() {
		float ambient[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float diffuse[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };

		myGL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		myAUX.auxSolidBox(0.8, 1.2, 0.5);
		myGL.glPopMatrix();

	}

	public void drawRightArm() {
		float ambient[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float diffuse[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		// end color init
		myGL.glPushMatrix();
		// SHOUDER
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		// end material
		myGL.glTranslated(0.4, 0.5f, 0.0f);
		myGL.glRotatef((float) -60 % 360, 0.0f, 0.0f, 1.0f);
		myGL.glRotatef((float) shoulderRight, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(0.3, 0.0, 0.0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		// ELBOW
		myGL.glPushMatrix();
		float ambient1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f }; // sang
		float specular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission1[] = { 198.0f / 255.0f, 119.0f / 255.0f, 76.0f / 255.0f, 1.0f }; // toi
		float shininess1[] = { 20 };
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission1);
		myGL.glTranslated(0.3, 0.0f, 0.0f);
		myGL.glRotatef((float) elbowRight, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(0.3, 0, 0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		myGL.glPopMatrix();
		// end Elbow

		myGL.glPopMatrix();
	}

	public void drawLeftArm() {
		float ambient[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float diffuse[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		// end color init
		myGL.glPushMatrix();
		// SHOUDER
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		// end material
		myGL.glTranslated(-0.4, 0.5f, 0.0f);
		myGL.glRotatef((float) 60 % 360, 0.0f, 0.0f, 1.0f);
		myGL.glRotatef((float) shoulderLeft, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(-0.3, 0.0, 0.0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		// ELBOW
		myGL.glPushMatrix();
		float ambient1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f }; // sang
		float specular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission1[] = { 198.0f / 255.0f, 119.0f / 255.0f, 76.0f / 255.0f, 1.0f }; // toi
		float shininess1[] = { 20 };
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission1);
		myGL.glTranslated(-0.3, 0.0f, 0.0f);
		myGL.glRotatef((float) elbowLeft, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(-0.3, 0, 0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		myGL.glPopMatrix();
		// end Elbow
		myGL.glPopMatrix();
	}

	public void drawLeftLeg() {
		float ambient[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 55.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		myGL.glPushMatrix();
		// DUI
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		myGL.glTranslated(-0.25, -0.6f, 0.0f);
		myGL.glRotatef((float) legLeft, xLeg, 0.0f, zLeg);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// CANG CHAN
		myGL.glTranslated(0, -0.4, 0.0);
		myGL.glRotatef((float) cangTrai, 1.0f, 0.0f, 0.0f);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// BAN CHAN
		float ambient2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float diffuse2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float specular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission2[] = { 71.0f / 255.0f, 71.0f / 255.0f, 71.0f / 255.0f, 1.0f };
		float shininess2[] = { 20 };
		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission2);
		myGL.glTranslated(0, -0.5, 0.1);
		myAUX.auxSolidBox(0.3, 0.2, 0.7);

		myGL.glPopMatrix();
		myGL.glPopMatrix();
	}

	public void drawRightLeg() {
		float ambient[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 55.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		myGL.glPushMatrix();
		// DUI
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		myGL.glTranslated(0.25, -0.6f, 0.0f);
		myGL.glRotatef((float) legRight, xLeg, 0.0f, zLeg);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// CANG CHAN
		myGL.glTranslated(0, -0.4, 0.0);
		myGL.glRotatef((float) cangPhai, 1.0f, 0.0f, 0.0f);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// BAN CHAN
		float ambient2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float diffuse2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float specular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission2[] = { 71.0f / 255.0f, 71.0f / 255.0f, 71.0f / 255.0f, 1.0f };
		float shininess2[] = { 20 };
		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission2);
		myGL.glTranslated(0, -0.5, 0.1);
		myAUX.auxSolidBox(0.3, 0.2, 0.7);

		myGL.glPopMatrix();
		myGL.glPopMatrix();
	}

	public void drawHead() {
		myGL.glPushMatrix();
		float ambient1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f }; // sang
		float specular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission1[] = { 198.0f / 255.0f, 119.0f / 255.0f, 76.0f / 255.0f, 1.0f }; // toi
		float shininess1[] = { 20 };
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission1);
		myGL.glTranslated(0, 0.85, 0);
		myUT.glutSolidSphere(0.25, 50, 50);
		myGL.glPopMatrix();
	}

	public void drawBall() {
		float mat_ambient[] = { 254.0f / 255.0f, 237.0f / 255.0f, 59.0f / 255.0f, 1.0f };
		float mat_diffuse[] = { 254.0f / 255.0f, 237.0f / 255.0f, 59.0f / 255.0f, 1.0f };
		float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float low_shininess[] = { 5.0f };
		float mat_emission[] = { 155.0f / 255.0f, 137.0f / 255.0f, 57.0f / 255.0f, 1.0f };
		myGL.glPushMatrix();
		myGL.glTranslated(-0.25, -2.2, 0.8);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, mat_ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, low_shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, mat_emission);
		myUT.glutSolidSphere(0.25, 50, 50);
		myGL.glPopMatrix();
	}

	/*
	 * END MY CODE
	 */

	private void myinit() {
		myGL.glShadeModel(GL.GL_FLAT);
		float ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		float diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float position[] = { 0.0f, 3.0f, 2.0f, 0.0f };
//		float lmodel_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		float local_view[] = { 0.0f };

		myGL.glEnable(GL.GL_DEPTH_TEST);
		myGL.glDepthFunc(GL.GL_LESS);

		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, ambient);
		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, diffuse);
		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, specular);
		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, position);
//		myGL.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
		myGL.glLightModelfv(GL.GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);

		myGL.glEnable(GL.GL_LIGHTING);
		myGL.glEnable(GL.GL_LIGHT0);

		myGL.glClearColor(0.0f, 0.1f, 0.1f, 0.0f);

	}

	public void componentMoved(ComponentEvent e) {
	}

	public void componentShown(ComponentEvent e) {
	}

	public void componentHidden(ComponentEvent e) {
	}

	public void componentResized(ComponentEvent e) {
		// get window width and height by myself
		myReshape(getSize().width, getSize().height);
		display();
		repaint();
	}

	private void myReshape(int w, int h) {
		myGL.glViewport(0, 0, w, h);
		myGL.glMatrixMode(GL.GL_PROJECTION);
		myGL.glLoadIdentity();
		myGLU.gluPerspective(60.0, (double) w / (double) h, 1.0, 20.0);
		myGLU.gluLookAt(look, 0, 2, 0, 0, 0, 0, 1, 0);
		myGL.glMatrixMode(GL.GL_MODELVIEW);
		myGL.glLoadIdentity();
		myGL.glTranslatef(0.0f, 0.0f, -5.0f);
	}

	public void update(Graphics g) {
		// skip the clear screen command....
		paint(g);
	}

	public void paint(Graphics g) {
		myGL.glXSwapBuffers(g, this);
	}

	public void init() {
		myAUX.auxInitPosition(0, 0, 400, 400);
		myAUX.auxInitWindow(this);
		myinit();

		// as call auxKeyFunc()
		addKeyListener(this);

		// as call auxReshapeFunc()
		addComponentListener(this);
		myReshape(getSize().width, getSize().height);

		// call display as call auxIdleFunc(display)
		display();
	}

}
